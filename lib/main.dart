import 'package:flutter/material.dart';
import 'package:flutter_form/screens/health_data.dart';
import 'package:flutter_form/screens/profile_form.dart';

void main() {
  runApp(
    MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => MyApp(),
        '/profile': (context) => ProfileForm(),
        '/healthPage': (context) => HealthForm(),
      },
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "CONTINUE WITH THE FORM",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w600,
                  color: Colors.black45,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/profile');
                },
                child: Text(
                  "PROCEED",
                  style: TextStyle(color: Colors.black54, fontSize: 30),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
