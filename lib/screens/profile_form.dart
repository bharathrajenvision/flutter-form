import 'package:flutter/material.dart';

class ProfileForm extends StatefulWidget {
  @override
  _ProfileFormState createState() => _ProfileFormState();
}

class _ProfileFormState extends State<ProfileForm> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                setState(
                  () {
                    Navigator.pop(context);
                  },
                );
              },
            ),
            title: Text(
              "Flutter Form",
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
            ),
          ),
          body: SafeArea(
            child: Container(
              color: Colors.blueGrey,
              // width: MediaQuery.of(context).size.width,
              // height: MediaQuery.of(context).size.height,
              // decoration: BoxDecoration(
              //   image: DecorationImage(
              //   image: AssetImage(),
              //   fit: Boxfit.cover,
              //   ),
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      // width: 100.0,
                      // height: 100.0,
                      child: Text(
                        "Profile Info",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 30.0,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 10,
                    child: Container(
                      // width: 100.0,
                      // height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.circular(30.0),
                        ),
                      ),
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 20, horizontal: 24),
                        child: ListView(
                          children: [
                            Text(
                              "Welcome to FlutterForm",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w600),
                            ),
                            SizedBox(
                              height: 14,
                            ),
                            Text(
                              "Proceed with the form",
                              style: TextStyle(
                                  fontSize: 15, color: Colors.black54),
                            ),
                            SizedBox(
                              height: 25,
                            ),
                            TextField(
                              style: TextStyle(
                                  fontSize: 14, color: Colors.black54),
                              decoration: InputDecoration(
                                hintText: "FIRST NAME",
                                labelStyle: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            TextField(
                              style: TextStyle(
                                  fontSize: 14, color: Colors.black54),
                              decoration: InputDecoration(
                                hintText: "LAST NAME",
                                labelStyle: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            TextField(
                              style: TextStyle(
                                  fontSize: 14, color: Colors.black54),
                              decoration: InputDecoration(
                                hintText: "DATE OF BIRTH",
                                labelStyle: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            TextField(
                              style: TextStyle(
                                  fontSize: 14, color: Colors.black54),
                              decoration: InputDecoration(
                                hintText: "EMAIL",
                                labelStyle: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            TextField(
                              style: TextStyle(
                                  fontSize: 14, color: Colors.black54),
                              decoration: InputDecoration(
                                hintText: "NEXT QUESTIONS",
                                labelStyle: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          bottomNavigationBar: ElevatedButton(
            child: Text(
              "NEXT",
              style: TextStyle(fontSize: 25, color: Colors.black87),
            ),
            onPressed: () {
              setState(() {
                Navigator.pushNamed(context, '/healthPage');
              });
            },
          ),
        ),
      ),
    );
  }
}
